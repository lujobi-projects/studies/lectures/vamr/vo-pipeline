function [newState, position] = processFrame(state, prev_img, image, prev_pos, K, params)
    % INPUT:
    %   - state: 1 = keypoints P, 
    %            2 = landmarks X, 
    %            3 = candidateKeypoints C, 
    %            4 = firstSeen F, 
    %            5 = firstSeenCamPose T 
    %   - prev_img: previous frame (where keypoints were found)
    %   - image: current frame
    %   - previous_pose: pose of last frame (used for possible reset)
    %   - K: Intrinsic Matrix of Camera (in the form used in the lecture)
    %   - params: tunable parameters for different datasets
    % RETURNS:
    %   - newState : new State (see above)
    %   - position : new position if camera at current frame
    
    % extract/create parameters used later
    cameraParams = cameraParameters('IntrinsicMatrix',K.');
    processParams = params.processFrame;
    bootstrap_param = params.bootstrapping;
    
    % track points from prev to current frame with KLT
    tracker = vision.PointTracker('NumPyramidLevels', processParams.KLT.num_pyramid_levels, ...
                                  'MaxBidirectionalError', processParams.KLT.max_bidirectional_error, ...
                                  'BlockSize', processParams.KLT.block_size, ...
                                  'MaxIterations', processParams.KLT.max_iterations);
    initialize(tracker, state{1}, prev_img);
    [points,validity] = tracker(image);
    release(tracker);
    
    % remove landmarks corresponding to keypoints which can't be matched
    newState{1} = points(validity,:);
    newState{2} = state{2}(validity,:);
    newState{3} = [];
    newState{4} = [];
    newState{5} = [];
    
    % previous camera position
    t_prev = prev_pos(1:3, 4);
    
    % if not enough keypoints could be tracked, reset
    if length(newState{1}) < processParams.KLT.min_matches
        disp("to less keypoints matched, resetting..")
        [state, newState, ~] = resetState(prev_pos, prev_img, image, K, bootstrap_param);
    end
    
    % estimate World pose using ransac and KLT
    [R_curr, t_curr, inlierIdx]= estimateWorldCameraPose(newState{1}, newState{2}, cameraParams, ...
        'MaxReprojectionError',processParams.eWCP.max_reprojection_error,...
        'MaxNumTrials', processParams.eWCP.max_num_trials,...
        'Confidence' , processParams.eWCP.confidence);
    t_curr = t_curr.';
    
    move_dist = pdist2(t_prev', t_curr');
    disp("Movement since last frame: " + string(move_dist));

    % remove Keypoints and landmarks which were declard as outliers during reprojection
    newState{1} = newState{1}(inlierIdx,:);
    newState{2} = newState{2}(inlierIdx,:);

    % optimize R and t w.r.t minimizing the reprojection error
    errors = @(x) (reprojectionError(x, newState{1}, newState{2}', K));
    x_init = [rotm2eul(R_curr)', t_curr]; % use euler angels in order to encode the rotation matrices
    options = optimoptions(@lsqnonlin, 'Display','off','MaxIter', processParams.optimization.max_iter);
    x_opt = lsqnonlin(errors, x_init, [], [], options);

    % update T, R, t from optimization
    R_curr = eul2rotm(x_opt(:,1)'); % convert back to rot matr.
    t_curr = x_opt(:,2);
    T_curr = [R_curr, t_curr];

    
    temp = - R_curr' * t_curr;
    move_dist = pdist2(t_prev', temp');
    disp("Movement after optimizing: " + string(move_dist));
    
    % calculate the average reprojection error
    avg_repr_err = sum(pdist2(errors(x_opt)', [0,0]), "all")/length(errors(x_init));
    avg_repr_err_old = sum(pdist2(errors(x_init)', [0,0]), "all")/length(errors(x_init));
    disp("Average reproject error: " + string(avg_repr_err_old))
    disp("Average reproject error after optimization: " + string(avg_repr_err))

    if avg_repr_err > processParams.optimization.max_repr_err || ...
       move_dist > processParams.eWCP.max_movement
        disp("too high reprojection error or jump detected, resetting..")
        [state, newState, position] = resetState(prev_pos, prev_img, image, K, bootstrap_param);
        T_curr = position;
    else

        % position of cam w.r.t. world frame
        position = [R_curr', - R_curr' * t_curr];

        % reject landmarks which moved behind the camera
        dist2cam = T_curr(:, 1:3) * newState{2}' + T_curr(:, 4);
        newState{1} = newState{1}(dist2cam(3, :) > 0, :);
        newState{2} = newState{2}(dist2cam(3, :) > 0, :);
    end

    % if we have candidate Keypoints
    if all(size(state{3}) ~= 0)
        % track the keypoints to current frame
        initialize(tracker, state{3}', prev_img);
        [points,validity] = tracker(image);
        release(tracker);
        
        % remove all invalid 
        newState{3} = points(validity,:)';
        newState{4}= state{4}(:,validity);
        newState{5}= state{5}(:,validity);
        
        % vector which contains logical values for new landmarks
        keypoint_selection = true(length(newState{3}), 1);
        
        for i = 1:size(newState{3}, 2) % for each keypoint
            T_first_seen = reshape(state{5}(:,i), [3, 4]);
            
            % projection matrices
            M1 = K * T_first_seen;
            M2  = K * T_curr;
            
            % triangulate 3D point using pose from current and first observation
            [lm_pose, ~, valid] = triangulate(newState{4}(:,i).', newState{3}(:,i)', M1.', M2.');
            lm_pose = lm_pose.';
            
            % world poses of camera
            c1_pose = T_first_seen * [lm_pose;1];
            c2_pose = T_curr * [lm_pose;1];
            c1_pose = c1_pose(1:3); 
            c2_pose = c2_pose(1:3);
                        
            
            if valid % in front of the camera
                % compute angle between vectors to landmark from first and
                % second pose
                alpha = atan2(norm(cross(c1_pose, c2_pose)), dot(c1_pose, c2_pose));
                if abs(alpha) >= processParams.landmarks.alpha_min && abs(alpha) <= processParams.landmarks.alpha_max
                    newState{1} = [newState{1}; newState{3}(:,i)'];
                    newState{2} = [newState{2}; lm_pose(1:3, :)'];
                    keypoint_selection(i) = false;
                end
            end       
        end
        %remove selected keypoints from candidates
        newState{3} = newState{3}(:, keypoint_selection);
        newState{4} = newState{4}(:, keypoint_selection);
        newState{5} = newState{5}(:, keypoint_selection);
    end
    
    old_keypoints = [newState{1}; newState{3}'];
    sz = size(image);
    % retrieve new candidates candidates: 2xN
    candidates = detectMinEigenFeatures(image,'ROI', [1,1,sz(2), sz(1)]);
    best_candidates = double(selectStrongest(candidates, processParams.new_candidate.count).Location);
    
    % Remove candidates which are too close to the already selected keypoints
    new_kp_ct = size(best_candidates, 1);
    selection = false(size(best_candidates, 1), 1);
    
    % all dists between points
    dist_candidate_keypts = pdist2(best_candidates, old_keypoints, 'squaredeuclidean');
    new_cand_Tol = processParams.new_candidate.dist^2;
    for i = 1:new_kp_ct
        if all(dist_candidate_keypts(i, :) > new_cand_Tol)
            selection(i) = true;
        end
    end
    
    % Add new Candidates to state
    newState{3} = [newState{3}, best_candidates(selection, :).'];
    newState{4} = [newState{4}, best_candidates(selection, :)'];
    newState{5} = [newState{5}, repmat(reshape(T_curr, [12, 1]), [1, nnz(selection)])];
end