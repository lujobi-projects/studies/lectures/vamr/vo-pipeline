function [inl_kp_0, inl_kp_1, landmarks, rel_pose] = init_loc(p0,p1,K)
    % selects new keypoints for matching
    % INPUT: 
    %   - p0: pose of keypoints in the first frame
    %   - p1: pose of keypoints in the second frame
    %   - K: Intrinsic Matrix of Camera (in the form used in the lecture)
    % RETURNS: 
    %   - inl_kp_0: pose of matched keypoints in the first frame
    %   - inl_kp_1: pose of matched keypoints in the second frame
    %   - landmarks: 3D position of landmarks w.r.t. camera pose at first frame 
    %   - rel_pose: relative 3D position of camera pose at frame 2
    %               w.r.t. camera pose at first frame 

    cameraParams = cameraParameters('IntrinsicMatrix',K.');
    [E,inliersIndex] = estimateEssentialMatrix(p0, p1, cameraParams);
    p0 = p0(inliersIndex, :);
    p1 = p1(inliersIndex, :);
    % calculate relative camera position
    [relR,relt] = relativeCameraPose(E,cameraParams,p0,p1);
     
    rel_pose = [relR',  -relR' * relt'];
    
    % calc Camera Matrix
    M0 = K*eye(3,4);
    M1 = K*rel_pose; 
    % triangulate points in 3D space
    [X_W, ~, valid] = triangulate(p0, p1, M0', M1');
    
    % only select landmarks in front of camera
    landmarks = X_W(valid, :);
    inl_kp_0 = p0(valid, :);
    inl_kp_1 = p1(valid, :);
end