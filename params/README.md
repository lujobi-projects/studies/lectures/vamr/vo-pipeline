# Explanation of all tunable parameters

## Bootstrapping

### Slection of candidates

* `param.bootstrapping.new_candidate.count` count of new keypoints to be selected, default: `600`

### KLT tracker (vision.PointTracker) used to track keypoints

* `param.bootstrapping.KLT.num_pyramid_levels` amount of pyramid levels used by KLT Tracker, default:  `4`
* `param.bootstrapping.KLT.max_bidirectional_error`, maximum allowed error between forward tracking then backward tracking the point and original keypoint, default: `6`
* `param.bootstrapping.KLT.block_size`, Size of neighborhood around each point being tracked, default: `[31 31]`
* `param.bootstrapping.KLT.max_iterations`, max. iterations of used RANSAC, default: `20`
* `param.bootstrapping.KLT.min_matches`, minimum count of matched keypoints after tracking, default: `10`

## Processing frame

### KLT tracker (vision.PointTracker) used to track existing keypoints

* `param.processFrame.KLT.num_pyramid_levels`, see above, default: `param.bootstrapping.KLT.num_pyramid_levels`
* `param.processFrame.KLT.max_bidirectional_error`, see above, default: `param.bootstrapping.KLT.max_bidirectional_error`
* `param.processFrame.KLT.block_size`, see above, default: `param.bootstrapping.KLT.block_size`
* `param.processFrame.KLT.max_iterations`, see above, default: `param.bootstrapping.KLT.max_iterations`
* `param.processFrame.KLT.min_matches`, see above, default: `param.bootstrapping.KLT.min_matches`

### EstimateWorldCameraPose

* `param.processFrame.eWCP.max_reprojection_error`, max. reprojection error of keypoint to be considered inlier, default: `1`
* `param.processFrame.eWCP.max_num_trials`, maximum numbers of trials to determine the pose, default: `1000`
* `param.processFrame.eWCP.confidence`, confidence of finding max amount of inliers, default: `99.5`
* `param.processFrame.eWCP.max_movement`, maximum allowed movement which doesn't trigger a reinitialization of keypoints, default: `3`

### Optimization of estimate

* `param.processFrame.optimization.max_repr_err`, maximum average reprojection error (over all keypoints), which doesn't trigger a reinitialization, default: `1`

### Selecting new landmarks

* `param.processFrame.landmarks.alpha_min`, min angle to between first seen vector and current vector to landmark to be considered as keypoint, default: `deg2rad(0.4)`
* `param.processFrame.landmarks.alpha_max`, max angle to be considered as keypoint, default: `deg2rad(10.0)`

### selecting new candidates

* `param.processFrame.new_candidate.dist`, min. distance (in pixels) a new candidate needs to have from all existing keypoints, in order to be selected as candidate, default: `8`
* `param.processFrame.new_candidate.count`, count of new candidates which can be selected from, default: `600`

## reset with a spacing of two frame (one otherwise)

* `param.reset.twoframes`, whether to reset with the previous frame (`false`) or the pose and frame which are an iteration older (`true`) default: `false` **Attention** `param.processFrame.eWCP.max_movement` per default counts the movement for one step in difference only.
